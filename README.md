# Traktor 1.9
Traktor will autamically install Tor, privoxy, dnscrypt-proxy and Tor Browser Launcher in either a Debian based distro like Ubuntu, an Arch based distro, Fedora based distro or an OpenSUSE based distro and configures them as well.

To do this, just run 'traktor.sh' file in a supported shell like bash and watch for prompts it asks you.

## Note
Do NOT expect anonymity using this method. Privoxy is an http proxy and can leak data. If you need anonymity or strong privacy, manually run torbrowser-launcher after installing traktor and use it.

## Manual Pages
[راهنمای اسکریپت تراکتور به زبان فارسی](https://soshaw.net/traktor/) {lang=FA}

## Install
### Ubuntu and other Distro
    wget https://gitlab.com/TraktorPlus/Traktor/repository/master/archive.zip?ref=master -O traktor.zip
    unzip traktor.zip -d  ~/Traktor && cd ~/Traktor/*
    ./traktor.sh
### ArchLinux
    yaourt -S traktor

## Uninstall
### Ubuntu and Debian other Debian Base Distro
    chmod +x uninstall_debian.sh
    ./uninstall_debian.sh
### OpenSUSE
    chmod +x uninstall_opensuse.sh
    ./uninstall_opensuse.sh
    
## Changes
[See Changes](https://gitlab.com/Sosha/traktor/blob/master/CHANGELOG)
