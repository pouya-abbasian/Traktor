#!/bin/bash

# License : GPLv3+


#Checking if the distro is Debian Base/ Arch Base/ Redhat Base/ OpenSUSE base and running the correct script
if pacman -Q &> /dev/null ;then
  if [ ! -f ./traktor_arch.sh ]; then
    wget -O ./traktor_arch.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_arch.sh' || curl -O  https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_arch.sh
  fi
  sudo chmod +x ./traktor_arch.sh
 ./traktor_arch.sh
  # echo "arch"
elif apt list --installed &> /dev/null ;then
  if [ ! -f ./traktor_debian.sh ]; then
    wget -O ./traktor_debian.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_debian.sh' || curl -O  https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_debian.sh
  fi
  sudo chmod +x ./traktor_debian.sh
  ./traktor_debian.sh
  # echo "debian"
elif dnf list &> /dev/null ;then
  if [ ! -f ./traktor_fedora.sh ]; then
    wget -O ./traktor_fedora.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_fedora.sh' || curl -O  https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_fedora.sh
  fi
  sudo chmod +x ./traktor_fedora.sh
  ./traktor_fedora.sh
  # echo "fedora"
elif zypper search i+ &> /dev/null ;then
  if [ ! -f ./traktor_opensuse.sh ]; then
    wget -O ./traktor_opensuse.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_opensuse.sh' || curl -O  https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_opensuse.sh
  fi
  sudo chmod +x ./traktor_opensuse.sh
  ./traktor_opensuse.sh
  # echo "openSUSE"
else
    echo "Your distro is neither Debian Base nor Arch Base nor Redhat Base nor OpenSUSE Base So, The script is not going to work in your distro."
fi
if [ ! -f ./traktor.sh ]; then # if then -> detect remote install
  if [ -f ./traktor_arch.sh ]; then
    rm ./traktor_arch.sh
  fi
  if [ -f ./traktor_debian.sh ]; then
    rm ./traktor_debian.sh
  fi
  if [ -f ./traktor_fedora.sh ]; then
    rm ./traktor_fedora.sh
  fi
  if [ -f ./traktor_opensuse.sh ]; then
    rm ./traktor_opensuse.sh
  fi
fi